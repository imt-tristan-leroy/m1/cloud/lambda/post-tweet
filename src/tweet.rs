use chrono::{DateTime, Utc};
use std::time::SystemTime;

use aws_sdk_dynamodb::{model::AttributeValue, Client, Error as DynamoError};
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Tweet {
    pub channel_id: String,
    pub likes: u8,
    pub message: String,
    pub username: String,
    pub timestamp_utc_iso8601: String,
}

impl Tweet {
    pub fn new(channel_id: String, likes: u8, message: String, username: String) -> Self {
        let now = SystemTime::now();
        let now: DateTime<Utc> = now.into();

        Tweet {
            channel_id,
            likes,
            message,
            username,
            timestamp_utc_iso8601: now.to_rfc3339(),
        }
    }
}

pub async fn add_tweet(client: &Client, table: &str, tweet: Tweet) -> Result<Tweet, DynamoError> {
    let _response = client
        .put_item()
        .table_name(table)
        .item(
            "channel_id",
            AttributeValue::S(tweet.channel_id.to_string()),
        )
        .item(
            "timestamp_utc_iso8601",
            AttributeValue::S(tweet.timestamp_utc_iso8601.to_string()),
        )
        .item("likes", AttributeValue::N(tweet.likes.to_string()))
        .item("message", AttributeValue::S(tweet.message.to_string()))
        .item("username", AttributeValue::S(tweet.username.to_string()))
        .send()
        .await?;

    Ok(tweet)
}
