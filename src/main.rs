pub mod tweet;
mod user;

use lambda_http::{run, service_fn, Body, Error, Request, Response};
use tracing::info;

use aws_sdk_dynamodb::Client;
use tweet::{add_tweet, Tweet};

use crate::user::get_user;

#[derive(Clone, Debug, serde::Deserialize, serde::Serialize)]
struct RequestBody {
    message: String,
}

/// This is the main body for the function.
/// Write your code inside it.
/// There are some code example in the following URLs:
/// - https://github.com/awslabs/aws-lambda-rust-runtime/tree/main/examples
async fn function_handler(event: Request) -> Result<Response<Body>, Error> {
    let headers = event.headers();
    let authorization = headers.get("Authorization");
    let token = match authorization {
        Some(t) => t.to_str().unwrap().strip_prefix("Bearer "),
        None => {
            let resp = Response::builder()
                .status(401)
                .header("content-type", "application/json")
                .body(Body::Empty)
                .map_err(Box::new)?;
            return Ok(resp);
        }
    };

    let user = match get_user(token.unwrap().to_string()).await {
        Ok(u) => u,
        Err(err) => {
            let resp = Response::builder()
                .status(match err.status() {
                    Some(s) => s.as_u16(),
                    None => 500,
                })
                .header("content-type", "application/json")
                .body(err.to_string().into())
                .map_err(Box::new)?;
            return Ok(resp);
        }
    };

    let body = event.body();
    let s = std::str::from_utf8(&body).expect("invalid utf-8 sequence");
    //Log into Cloudwatch
    info!(payload = %s, "JSON Payload received");

    //Serialze JSON into struct.
    //If JSON is incorrect, send back 400 with error.
    let item = match serde_json::from_str::<RequestBody>(s) {
        Ok(item) => item,
        Err(err) => {
            let resp = Response::builder()
                .status(400)
                .header("content-type", "application/json")
                .body(err.to_string().into())
                .map_err(Box::new)?;
            return Ok(resp);
        }
    };

    let config = aws_config::load_from_env().await;

    let client = Client::new(&config);

    let tweet = Tweet::new("rainbow".to_string(), 0, item.message, user.username);

    let created_tweet = add_tweet(&client, "dynamodb-all-messages", tweet).await?;

    let j = serde_json::to_string(&created_tweet)?;

    //Send back a 200 - success
    let resp = Response::builder()
        .status(200)
        .header("content-type", "application/json")
        .body(j.into())
        .map_err(Box::new)?;
    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::INFO)
        // disable printing the name of the module in every log line.
        .with_target(false)
        // disabling time is handy because CloudWatch will add the ingestion time.
        .without_time()
        .init();

    run(service_fn(function_handler)).await
}
