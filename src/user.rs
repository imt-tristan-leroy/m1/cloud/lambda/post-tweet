use reqwest;
use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct User {
    pub sub: String,
    pub email_verified: String,
    pub email: String,
    pub username: String,
}

pub async fn get_user(token: String) -> Result<User, reqwest::Error> {
    let client = reqwest::Client::new();
    let resp = client
        .get("https://rainbow.auth.eu-west-1.amazoncognito.com/oauth2/userInfo")
        .header("Authorization", "Bearer ".to_string() + &token)
        .send()
        .await?
        .json::<User>()
        .await?;

    Ok(resp)
}
